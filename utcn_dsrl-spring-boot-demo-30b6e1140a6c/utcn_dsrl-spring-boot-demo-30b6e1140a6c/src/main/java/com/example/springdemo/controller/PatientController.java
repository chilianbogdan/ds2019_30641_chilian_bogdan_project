package com.example.springdemo.controller;


import com.example.springdemo.dto.PatientDTO;
import com.example.springdemo.dto.PatientViewDTO;
import com.example.springdemo.dto.PatientWithItemsDTO;
import com.example.springdemo.services.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/Patient")
public class PatientController {

    private final PatientService patientService;

    @Autowired
    public PatientController(PatientService patientService) {
        this.patientService = patientService;
    }

    @GetMapping(value = "/{id}")
    public PatientViewDTO findById(@PathVariable("id") Integer id){
        return patientService.findPatientById(id);
    }

    @GetMapping()
    public List<PatientViewDTO> findAll(){
        return patientService.findAll();
    }

    @GetMapping(value = "/items")
    public List<PatientWithItemsDTO> findAllWithItems(){
        return patientService.findAllFetch();
    }

    @GetMapping(value = "/items/wrong")
    public List<PatientWithItemsDTO> findAllWithItemsWrong(){
        return patientService.findAllFetchWrong();
    }

    @PostMapping()
    public Integer insertPatientDTO(@RequestBody PatientDTO patientDTO){
        return patientService.insert(patientDTO);
    }

    @PutMapping()
    public Integer updatePatient(@RequestBody PatientDTO patientDTO) {
        return patientService.update(patientDTO);
    }

    @DeleteMapping()
    public void delete(@RequestBody PatientViewDTO patientViewDTO){
        patientService.delete(patientViewDTO);
    }
}
