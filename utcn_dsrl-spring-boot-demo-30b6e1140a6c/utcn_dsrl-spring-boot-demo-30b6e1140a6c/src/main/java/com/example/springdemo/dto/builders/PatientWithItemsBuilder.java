package com.example.springdemo.dto.builders;

import com.example.springdemo.dto.ItemDTO;
import com.example.springdemo.dto.PatientWithItemsDTO;
import com.example.springdemo.entities.Item;
import com.example.springdemo.entities.Patient;

import java.util.List;
import java.util.stream.Collectors;

public class PatientWithItemsBuilder {
    private PatientWithItemsBuilder(){}

    public static PatientWithItemsDTO generateDTOFromEntity(Patient patient, List<Item> items){
        List<ItemDTO> dtos =  items.stream()
                .map(ItemBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());

        return new PatientWithItemsDTO(
                patient.getId(),
                patient.getName(),
                dtos);
    }

}
