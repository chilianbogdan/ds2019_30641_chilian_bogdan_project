package com.example.springdemo.dto;

import java.util.Objects;

public class PatientDTO {

    private Integer id;
    private String name;
    private String birthdate;
    private String gender;
    private String address;
    private String medicalrecord;

    public PatientDTO() {
    }

    public PatientDTO(Integer id, String name, String birthdate, String gender, String address, String medicalrecord) {
        this.id = id;
        this.name = name;
        this.birthdate = birthdate;
        this.gender = gender;
        this.address = address;
        this.medicalrecord = medicalrecord;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMedicalrecord() {
        return medicalrecord;
    }

    public void setMedicalrecord(String medicalrecord) {
        this.medicalrecord = medicalrecord;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PatientDTO patientDTO = (PatientDTO) o;
        return Objects.equals(id, patientDTO.id) &&
                Objects.equals(name, patientDTO.name) &&
                Objects.equals(birthdate, patientDTO.birthdate) &&
                Objects.equals(gender, patientDTO.gender) &&
                Objects.equals(address, patientDTO.address) &&
                Objects.equals(medicalrecord, patientDTO.medicalrecord);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, name, birthdate, gender, address, medicalrecord);
    }
}
