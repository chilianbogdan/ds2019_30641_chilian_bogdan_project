package com.javasampleapproach.rabbitmq.producer;

import com.javasampleapproach.rabbitmq.jsonconversion.CustomMessage;
import com.javasampleapproach.rabbitmq.jsonconversion.ReadFileToList;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Component
public class Producer {
	
	@Autowired
	private AmqpTemplate amqpTemplate;
	
	@Value("${jsa.rabbitmq.exchange}")
	private String exchange;
	
	@Value("${jsa.rabbitmq.routingkey}")
	private String routingKey;

	File file = new File("E:\\Facultate\\SD\\Assignment2\\file.txt");



	BufferedReader abc = new BufferedReader(new FileReader(file));
	List<String> lines = new ArrayList<String>();
	CustomMessage[] act = new CustomMessage[lines.size()];
	ReadFileToList rf = new ReadFileToList();
	public Producer() throws FileNotFoundException {
	}

	public void produceMsg(String msg) throws InterruptedException, IOException {

		//ApplicationContext ctx = new AnnotationConfigApplicationContext(RabbitMqConfig.class);
		//AmqpTemplate amqpTemplate = ctx.getBean(AmqpTemplate.class);
		//CustomMessage[] act = new CustomMessage;
		//AtomicInteger counter = new AtomicInteger();d

		act = rf.rd(abc,lines);
		for (int i = 0; i < act.length; i++){
			//CustomMessage ms = new CustomMessage( 1, "RabbitMQ Spring JSON Example",  "st", "end");
			msg= act[i].toString();
			amqpTemplate.convertAndSend(exchange, routingKey, msg);
			System.out.println("Send msg = " + msg);
			TimeUnit.SECONDS.sleep(1);
		}
		act = null;
		abc = new BufferedReader(new FileReader(file));

	}
}

