package com.codenotfound.grpc.helloworld;

import javax.annotation.PostConstruct;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;

import java.util.logging.Logger;

public class HelloWorldClient {


    private HelloWorldServiceGrpc.HelloWorldServiceBlockingStub helloWorldServiceBlockingStub;

    public String sayHello(String firstName, String lastName) {
        ManagedChannel managedChannel = ManagedChannelBuilder
                .forAddress("localhost", 6565).usePlaintext().build();

        helloWorldServiceBlockingStub =
                HelloWorldServiceGrpc.newBlockingStub(managedChannel);

        Person person = Person.newBuilder().setFirstName(firstName)
                .setLastName(lastName).build();
        //LOGGER.info("client sending {}", person);
        System.out.println("client sending {}" + person);

        Greeting greeting = Greeting.newBuilder().build();
        try{
            greeting =
                    helloWorldServiceBlockingStub.sayHello(person);
        } catch (Exception e) {
            //LOGGER.info("gets Exception");
            return greeting.getMessage();
        }
        //LOGGER.info("client received {}", greeting);
        System.out.println("client received {}" + greeting);

        return greeting.getMessage();
    }
}

