package com.codenotfound.grpc.helloworld;
import Time.CurentTime;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class Main {

    static int timer = 0;

    public static void test()
    {
            timer++;
        System.out.println("Timer: " +timer);
    }

    public static void main(String[] args) {

        new GUI();
        TimerTask timer1 = new TimerTask() {
            @Override
            public void run() {
                test();
            }
        };

        Timer t = new Timer();
        t.schedule(timer1, 1000,1000);

        Date currentDate = new Date();
        System.out.println("Time: " +currentDate);

        HelloWorldClient helloWorldClient = new HelloWorldClient();

        String message = helloWorldClient.sayHello("Medication", "Plan");

        System.out.println("Message received in main: "+message);

    }

}
