package com.codenotfound.grpc.helloworld.springdemo.controller;

import com.codenotfound.grpc.helloworld.springdemo.dto.MedDTO;
import com.codenotfound.grpc.helloworld.springdemo.dto.MedViewDTO;
import com.codenotfound.grpc.helloworld.springdemo.services.MedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/med")
public class MedController {
    private final MedService medService;



    @Autowired
    public MedController(MedService medService) {
        this.medService = medService;
    }

    //TODO: to be implemented
    @GetMapping(value = "/{id}")
    public MedViewDTO findById(@PathVariable("id") Integer id){
        return medService.findMedById(id);
    }

    @GetMapping()
    public List<MedViewDTO> findAll(){
        return medService.findAll();
    }
}
