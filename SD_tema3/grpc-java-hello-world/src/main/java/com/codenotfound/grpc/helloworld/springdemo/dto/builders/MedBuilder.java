package com.codenotfound.grpc.helloworld.springdemo.dto.builders;

import com.codenotfound.grpc.helloworld.springdemo.dto.MedDTO;
import com.codenotfound.grpc.helloworld.springdemo.entities.Med;

public class MedBuilder {
    private MedBuilder() {
    }

    public static MedDTO generateDTOFromEntity(Med med) {
        return new MedDTO(
                med.getId(),
                med.getName(),
                med.getSide(),
                med.getDosage());
    }

    public static Med generateEntityFromDTO(MedDTO medDTO) {
        return new Med(
                medDTO.getId(),
                medDTO.getName(),
                medDTO.getSide(),
                medDTO.getDosage());
    }
}
