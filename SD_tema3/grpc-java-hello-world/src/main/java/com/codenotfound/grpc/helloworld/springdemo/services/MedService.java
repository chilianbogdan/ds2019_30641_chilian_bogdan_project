package com.codenotfound.grpc.helloworld.springdemo.services;

import com.codenotfound.grpc.helloworld.springdemo.dto.MedDTO;
import com.codenotfound.grpc.helloworld.springdemo.dto.MedViewDTO;
import com.codenotfound.grpc.helloworld.springdemo.dto.builders.MedViewBuilder;
import com.codenotfound.grpc.helloworld.springdemo.entities.Med;
import com.codenotfound.grpc.helloworld.springdemo.repositories.MedRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@Service
public class MedService {
    private final MedRepository medRepository;

    @Autowired
    public MedService(MedRepository medRepository) {
        this.medRepository = medRepository;
    }

    //TODO: to be implemented

    public MedViewDTO findMedById(Integer id){
        Optional<Med> doctor  = medRepository.findById(id);


        return MedViewBuilder.generateDTOFromEntity(doctor.get());
    }
    public List<MedViewDTO> findAll(){
        List<Med> meds = medRepository.findAll();

        return meds.stream()
                .map(MedViewBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }
}
