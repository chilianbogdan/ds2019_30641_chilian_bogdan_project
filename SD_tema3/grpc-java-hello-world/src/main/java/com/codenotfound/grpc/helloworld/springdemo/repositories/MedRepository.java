package com.codenotfound.grpc.helloworld.springdemo.repositories;

import com.codenotfound.grpc.helloworld.springdemo.entities.Med;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MedRepository extends JpaRepository<Med, Integer> {

    Med findByName(String name);

    @Query(value = "SELECT u " +
            "FROM Med u " +
            "ORDER BY u.name")
    List<Med> getAllOrdered();


   // @Query(value = "SELECT p " +
   //         "FROM Medt p " +
   //         "INNER JOIN FETCH p.meds i"
   // )
   // List<Med> getAllFetch();

}
