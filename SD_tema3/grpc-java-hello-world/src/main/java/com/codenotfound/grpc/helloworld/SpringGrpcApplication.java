package com.codenotfound.grpc.helloworld;

import com.codenotfound.grpc.helloworld.springdemo.controller.MedController;
import com.codenotfound.grpc.helloworld.springdemo.dto.MedDTO;
import com.codenotfound.grpc.helloworld.springdemo.dto.MedViewDTO;
import com.codenotfound.grpc.helloworld.springdemo.repositories.MedRepository;
import com.codenotfound.grpc.helloworld.springdemo.services.MedService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

@SpringBootApplication
public class SpringGrpcApplication {

    public static void main(String[] args) {
       // List<MedViewDTO> md= new ArrayList<MedViewDTO>();
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));

       // MedRepository medRepository = null;
       // MedService medService = new MedService(medRepository);
        //MedController ctrl = new MedController(medService);
        //ctrl.findAll();
        //System.out.println(ctrl.findAll().toString());
        SpringApplication.run(SpringGrpcApplication.class, args);
    }
}
