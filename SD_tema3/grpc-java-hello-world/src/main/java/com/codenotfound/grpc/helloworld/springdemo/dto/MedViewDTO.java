package com.codenotfound.grpc.helloworld.springdemo.dto;

public class MedViewDTO {
    private Integer id;
    private String name;
    private Integer dosage;
    private String side;

    public MedViewDTO() {
    }

    public MedViewDTO(Integer id, String name, String side, int dosage) {
        this.id = id;
        this.name = name;
        this.side = side;
        this.dosage = dosage;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSide() {
        return side;
    }

    public void setSide(String side) {
        this.side = side;
    }

    public Integer getDosage() {
        return dosage;
    }

    public void setDosage(Integer dosage) {
        this.dosage = dosage;
    }
}
