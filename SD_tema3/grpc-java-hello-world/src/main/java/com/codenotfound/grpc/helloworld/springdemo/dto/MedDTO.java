package com.codenotfound.grpc.helloworld.springdemo.dto;

public class MedDTO {
    private Integer id;
    private String name;
    private Integer dosage;
    private String side;

    public MedDTO() {
    }

    public MedDTO(Integer id, String name, String side, int dosage) {
        this.id = id;
        this.name = name;
        this.side = side;
        this.dosage = dosage;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSide() {
        return side;
    }

    public void setSide(String side) {
        this.side = side;
    }

    public Integer getDosage() {
        return dosage;
    }

    public void setDosage(Integer dosage) {
        this.dosage = dosage;
    }
    public String toString(){//overriding the toString() method
        return id +" "+name+" "+dosage +" " + side;
    }
}
