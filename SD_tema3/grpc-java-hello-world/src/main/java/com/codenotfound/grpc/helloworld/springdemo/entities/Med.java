package com.codenotfound.grpc.helloworld.springdemo.entities;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;


@Entity
@Table(name = "med")
public class Med {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "SideEfects")
    private String side;

    @Column(name = "dosage")
    private int dosage;

    @ManyToOne()
    @JoinColumn(name = "patientid",  nullable = false)
    private Patient patient;

    public Med() {
    }

    public Med(Integer id, String name, String side, int dosage ) {
        this.id = id;
        this.name = name;
        this.side = side;
        this.dosage = dosage;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSide() {
        return side;
    }

    public void setSide(String side) {
        this.side = side;
    }

    public Integer getDosage() {
        return dosage;
    }

    public void setDosage(Integer dosage) {
        this.dosage = dosage;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }
}
