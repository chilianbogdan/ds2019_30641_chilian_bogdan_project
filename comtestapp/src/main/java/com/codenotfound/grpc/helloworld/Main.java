package com.codenotfound.grpc.helloworld;

public class Main {

    public static void main(String[] args) {
        HelloWorldClient helloWorldClient = new HelloWorldClient();

        String message = helloWorldClient.sayHello("Teodorus", "Moldovanus");

        System.out.println("Message received in main: "+message);
    }

}
