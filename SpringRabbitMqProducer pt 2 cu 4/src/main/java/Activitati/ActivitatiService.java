package Activitati;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repositoriesss.ActivitatiRepository;

@Service

public class ActivitatiService {

    @Autowired
    private ActivitatiRepository repo;

    public Iterable<Activitati> listAll()
    {

        return repo.findAll();
    }

    public void save(Activitati t)
    {

        repo.save(t);
    }

    public Activitati get(Long id)
    {
        return repo.findById(Math.toIntExact(id)).get();
    }


}
