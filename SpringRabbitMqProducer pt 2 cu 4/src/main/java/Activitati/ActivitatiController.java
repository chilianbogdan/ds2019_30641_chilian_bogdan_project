package Activitati;


import com.sun.org.apache.xerces.internal.impl.xpath.regex.ParseException;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.IOException;

@Controller
public class ActivitatiController {
    @Autowired
    private ActivitatiService service;

    @RequestMapping("/")
    public void viewHomePage(Model model) throws ParseException, JSONException, IOException, org.json.simple.parser.ParseException, java.text.ParseException {
        Iterable<Activitati> listT = service.listAll();
        model.addAttribute("listT", listT);


    }
}
