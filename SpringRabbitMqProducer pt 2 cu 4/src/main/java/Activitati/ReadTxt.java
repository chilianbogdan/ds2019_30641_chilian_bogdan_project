package Activitati;


import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class ReadTxt {
    public ReadTxt() throws FileNotFoundException
    {

    }

    public Activitati[] rd(BufferedReader buf, List<String> lines) throws IOException {
        String line;
        lines.clear();
        while ((line =  buf.readLine()) != null){
            lines.add(line);
        }
        buf.close();

        Activitati[] a = new Activitati[lines.size()];
        for(int i = 0; i < lines.size(); i++) {
            a[i] = new Activitati(lines.get(i));
        }
        return a;
    }


}
