package Activitati;

import javax.persistence.*;
import java.util.List;
import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "activitati")
public class Activitati {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private int patient_id;
    @Column(name = "activity")
    private String activity;
    @Column(name = "start")
    private String start;
    @Column(name = "end")
    private String end;


    public Activitati(int patient_id, String activity, String start, String end) {
        this.patient_id = patient_id;
        this.activity = activity;
        this.start = start;
        this.end = end;
    }
    public Activitati() {
        super();
        // TODO Auto-generated constructor stub
    }


    public int getPatient_id() {
        return patient_id;
    }

    public String getActivity() {
        return activity;
    }

    public String getStart() {
        return start;
    }

    public String getEnd() {
        return end;
    }

    public void setPatient_id(int patient_id) {
        this.patient_id = patient_id;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public void setEnd(String end) {
        this.end = end;
    }


    public Activitati(String parse)
    {
        String[] split = parse.split("\\t+");
        patient_id = 1;
        start = split[0];
        end = split[1];
        activity = split[2];
    }


    @Override
    public String toString() {
        return "{\n" +
                "\"patient_id=\": " + patient_id + ",\n" +
                "\"activity\": \"" + activity + "\",\n" +
                "\"start\": \"" + start + "\",\n" +
                "\"end\": \"" + end + "\"\n" +
                '}';
    }


}
