package Gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileReader;

public class Caregiver {
    private JList list1;
    private JLabel care;
    private JPanel lista;
    private JButton searchButton;
    DefaultListModel<String> listModel;
    boolean flag;

    public Caregiver() {
        initComponents();
        listModel = new DefaultListModel<>();
        flag = true;

    }

    public void initComponents() {

        JFrame jr = new JFrame();
        jr.setTitle("Interfata");
        jr.setVisible(true);
        jr.setSize(1024, 1024);
        jr.setContentPane(this.lista);
        jr.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        jr.pack();
        jr.getContentPane().setBackground(Color.BLACK);

        searchButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (flag){
                    BufferedReader b = null;
                try {
                    b = new BufferedReader(new FileReader("List.txt"));
                    int val = Integer.parseInt(b.readLine());
                    for (int i = 0; i < val; i++) {
                        String s = b.readLine();
                        listModel.addElement(s);
                    }
                    list1.setModel(listModel);
                    flag = false;
                } catch (Exception q) {
                    System.out.println("" + q);
                } finally {
                    try {
                        b.close();
                    } catch (Exception r) {
                        System.out.println("" + r);
                    }
                }
            }
            }
        });

    }
}
