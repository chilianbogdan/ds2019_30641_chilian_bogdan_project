package Gui;

import Activitati.Activitati;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class interfata {
    private JButton jButton_Search;
    private JTextField jText_Search;
    private JTable jTable_Users;
    private JPanel Activitati;
    private JButton jButton_Search1;
    private JTextField jText_Search1;
    private JTable table1;
    private JButton jButton_Search2;
    private JTextField jText_Search2;
    private JTable table2;
    private JList list1;
    private JTextField textField1;
    private JButton addListButton;
    private JButton adaugaInFisierButton;

    DefaultListModel<String> listModel;

    public interfata()
    {
        initComponents();
        listModel = new DefaultListModel<>();


    }

    public void initComponents() {

        JFrame jr = new JFrame();
        jr.setTitle("Interfata");
        jr.setVisible(true);
        jr.setSize(1024,1024);
        jr.setContentPane(this.Activitati);
        jr.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        jr.pack();
        jr.getContentPane().setBackground(Color.BLACK);
        jButton_Search.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                findUsers();
            }
        });

        jButton_Search1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                findUsers1();
            }
        });

        jButton_Search2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Gresit1();
            }
        });

        addListButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String s = textField1.getText();
                if(s.equals("")|| s.equalsIgnoreCase("Type"))
                {
                    textField1.setText("Type");
                }else
                {
                    listModel.addElement(s);
                    list1.setModel(listModel);
                    textField1.setText("");
                }
            }
        });

        adaugaInFisierButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int val = list1.getModel().getSize();
                        PrintWriter w = null;
                try{
                    w = new PrintWriter("List.txt");
                    w.println(val);
                    for(int i = 0; i<val;i++)
                    {

                        w.println(list1.getModel().getElementAt(i));
                    }
                }catch(Exception q){
                    System.out.print(""+q);
                }finally {
                    {
                        w.close();
                    }
                }
            }
        });


    }


    public Connection getConnection()
    {
        Connection con = null;

        try{
            con = DriverManager.getConnection("jdbc:mysql://localhost/springdemo","root","blaster");
        }catch(Exception ex){
            System.out.println(ex.getMessage());
        }
        return con;
    }

    public ArrayList<Activitati> ListUsers(String ValToSearch)
    {
        ArrayList<Activitati> usersList = new ArrayList<Activitati>();

        Statement st;
        ResultSet rs;

        try{
            Connection con = getConnection();
            st = con.createStatement();
            String searchQuery = "SELECT * FROM `activitati` WHERE CONCAT(`id`, `start`, `end`, `activity`) LIKE '%"+ValToSearch+"%'";
            rs = st.executeQuery(searchQuery);

            Activitati user;

            while(rs.next())
            {
                user = new Activitati(
                        rs.getInt("id"),
                        rs.getString("start"),
                        rs.getString("end"),
                        rs.getString("activity")
                );
                usersList.add(user);
            }

        }catch(Exception ex){
            System.out.println(ex.getMessage());
        }

        return usersList;
    }

    public ArrayList<Activitati> ListUsers1(String ValToSearch)
    {
        ArrayList<Activitati> usersList = new ArrayList<Activitati>();

        Statement st;
        ResultSet rs;

        try{
            Connection con = getConnection();
            st = con.createStatement();
            String searchQuery = "SELECT * FROM `bune` WHERE CONCAT(`id`, `start`, `end`, `activity`) LIKE '%"+ValToSearch+"%'";
            rs = st.executeQuery(searchQuery);

            Activitati user;

            while(rs.next())
            {
                user = new Activitati(
                        rs.getInt("id"),
                        rs.getString("start"),
                        rs.getString("end"),
                        rs.getString("activity")
                );
                usersList.add(user);
            }

        }catch(Exception ex){
            System.out.println(ex.getMessage());
        }

        return usersList;
    }


    public ArrayList<Activitati> Gresit(String ValToSearch)
    {
        ArrayList<Activitati> usersList1 = new ArrayList<Activitati>();

        Statement st;
        ResultSet rs;

        try{
            Connection con = getConnection();
            st = con.createStatement();
            String searchQuery = "SELECT * FROM `gresit` WHERE CONCAT(`id`, `start`, `end`, `activity`) LIKE '%"+ValToSearch+"%'";
            rs = st.executeQuery(searchQuery);

            Activitati user;

            while(rs.next())
            {
                user = new Activitati(
                        rs.getInt("id"),
                        rs.getString("start"),
                        rs.getString("end"),
                        rs.getString("activity")
                );
                usersList1.add(user);
            }

        }catch(Exception ex){
            System.out.println(ex.getMessage());
        }

        return usersList1;
    }

    public void findUsers()
    {
        ArrayList<Activitati> users = ListUsers(jText_Search.getText());
        DefaultTableModel model = new DefaultTableModel();
        model.setColumnIdentifiers(new Object[]{"id","start","end","activity"});
        Object[] row = new Object[4];

        for(int i = 0; i < users.size(); i++)
        {
            row[0] = users.get(i). getPatient_id();
            row[1] = users.get(i).getStart();
            row[2] = users.get(i).getEnd();
            row[3] = users.get(i).getActivity();
            model.addRow(row);
        }
        jTable_Users.setModel(model);

    }

    public void findUsers1()
    {
        ArrayList<Activitati> users = ListUsers1(jText_Search1.getText());
        DefaultTableModel model = new DefaultTableModel();
        model.setColumnIdentifiers(new Object[]{"id","start","end","activity"});
        Object[] row = new Object[4];

        for(int i = 0; i < users.size(); i++)
        {
            row[0] = users.get(i). getPatient_id();
            row[1] = users.get(i).getStart();
            row[2] = users.get(i).getEnd();
            row[3] = users.get(i).getActivity();
            model.addRow(row);
        }
        table1.setModel(model);

    }

    public void Gresit1()
    {
        ArrayList<Activitati> users =  Gresit(jText_Search2.getText());
        DefaultTableModel model1 = new DefaultTableModel();
        model1.setColumnIdentifiers(new Object[]{"id","start","end","activity"});
        Object[] row = new Object[4];

        for(int i = 0; i < users.size(); i++)
        {
            row[0] = users.get(i). getPatient_id();
            row[1] = users.get(i).getStart();
            row[2] = users.get(i).getEnd();
            row[3] = users.get(i).getActivity();
            model1.addRow(row);
        }
        table2.setModel(model1);

    }

   /* public static void main(String[] args) {

        interfata i = new interfata();
        login l = new login();

    }*/


}
