package dto.builders;

import dto.CaregiverViewDTO;
import com.javasampleapproach.rabbitmq.producer.Caregiver;

public class CaregiverViewBuilder {
    private CaregiverViewBuilder(){}

    public static CaregiverViewDTO generateDTOFromEntity(Caregiver patient){
        return new CaregiverViewDTO(
                patient.getId(),
                patient.getName(),
                patient.getBirth(),
                patient.getGender(),
                patient.getAddress(),
                patient.getPatients());
    }

    public static Caregiver generateEntityFromDTO(CaregiverViewDTO patientViewDTO){
        return new Caregiver(
                patientViewDTO.getId(),
                patientViewDTO.getName(),
                patientViewDTO.getBirth(),
                patientViewDTO.getGender(),
                patientViewDTO.getAddress(),
                patientViewDTO.getPatients());
    }
}
