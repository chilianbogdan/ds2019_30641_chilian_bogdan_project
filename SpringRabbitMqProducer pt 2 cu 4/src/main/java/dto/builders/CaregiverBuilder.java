package dto.builders;

import dto.CaregiverDTO;
import com.javasampleapproach.rabbitmq.producer.Caregiver;

public class CaregiverBuilder {

    private CaregiverBuilder() {
    }

    public static CaregiverDTO generateDTOFromEntity(Caregiver patient){
        return new CaregiverDTO(
                patient.getId(),
                patient.getName(),
                patient.getBirth(),
                patient.getGender(),
                patient.getAddress(),
                patient.getPatients());

    }

    public static Caregiver generateEntityFromDTO(CaregiverDTO patientDTO){
        return new Caregiver(
                patientDTO.getId(),
                patientDTO.getName(),
                patientDTO.getBirth(),
                patientDTO.getGender(),
                patientDTO.getAddress(),
                patientDTO.getPatients());

    }
}
