package dto.builders;

import dto.MedDTO;
import com.javasampleapproach.rabbitmq.producer.Med;

public class MedBuilder {
    private MedBuilder() {
    }

    public static MedDTO generateDTOFromEntity(Med med) {
        return new MedDTO(
                med.getId(),
                med.getName());
    }

    public static Med generateEntityFromDTO(MedDTO medDTO) {
        return new Med(
                medDTO.getId(),
                medDTO.getName());
    }
}
