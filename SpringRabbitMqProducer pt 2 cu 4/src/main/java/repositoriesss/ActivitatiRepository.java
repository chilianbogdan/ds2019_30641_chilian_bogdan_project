package repositoriesss;

import Activitati.Activitati;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ActivitatiRepository extends CrudRepository<Activitati, Integer> {

}
