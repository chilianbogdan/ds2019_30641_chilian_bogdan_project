package com.javasampleapproach.rabbitmq.producer;

import Activitati.Activitati;
import Activitati.ReadTxt;
import com.rabbitmq.client.ConnectionFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
@Component
public class Producer {

	@Autowired
	private AmqpTemplate amqpTemplate;
	
	@Value("${jsa.rabbitmq.exchange}")
	private String exchange;
	
	@Value("${jsa.rabbitmq.routingkey}")
	private String routingKey;

	public Jackson2JsonMessageConverter jsonMessageConverter(){
		return new Jackson2JsonMessageConverter();
	}

	public AmqpTemplate rabbitTemplate(ConnectionFactory connectionFactory) {
		final RabbitTemplate rabbitTemplate = new RabbitTemplate((org.springframework.amqp.rabbit.connection.ConnectionFactory) connectionFactory);
		rabbitTemplate.setMessageConverter((org.springframework.amqp.support.converter.MessageConverter) jsonMessageConverter());
		return rabbitTemplate;
	}

	File file = new File("C:/Users/Cuci/Desktop/SD-PR2/activity.txt");
	BufferedReader abc = new BufferedReader(new FileReader(file));
	List<String> lines = new ArrayList<String>();
	Activitati[] act = new Activitati[lines.size()];
	ReadTxt rf = new ReadTxt();
	public Producer() throws FileNotFoundException {
	}
	public void produceMsg(String msg) throws InterruptedException, IOException {

		act = rf.rd(abc,lines);
		for (int i = 0; i < act.length; i++){

				msg = act[i].toString();
				amqpTemplate.convertAndSend(exchange, routingKey, msg);
				System.out.println("Send msg = " + msg);
				//TimeUnit.SECONDS.sleep(1);

		}
		act = null;
		abc = new BufferedReader(new FileReader(file));


	}

}














