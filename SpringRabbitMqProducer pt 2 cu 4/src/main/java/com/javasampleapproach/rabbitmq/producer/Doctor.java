package com.javasampleapproach.rabbitmq.producer;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "doctor")
public class Doctor {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "pass")
    private String pass;


    public Doctor() {
    }

    public Doctor(Integer id, String name, String pass) {
        this.id = id;
        this.name = name;
        this.pass= pass;


    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPass() {
        return pass;
    }

    public void setBirth(String pass) {
        this.pass = pass;
    }



}
