package com.javasampleapproach.rabbitmq;

import Gui.interfata;
import Gui.login;
import com.javasampleapproach.rabbitmq.producer.Person;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.xml.namespace.QName;
import javax.xml.ws.Endpoint;
import javax.xml.ws.Service;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;

@SpringBootApplication
public class SOAPPublisherClient {

    public static void main(String[] args) throws MalformedURLException {

        login l = new login();

        Endpoint.publish("http://localhost:8888/ws/person", new PersonServiceImpl());

        SpringApplication.run(SpringRabbitMqProducerApplication.class, args);
        URL wsdlURL = new URL("http://localhost:8888/ws/person?wsdl");
        //check above URL in browser, you should see WSDL file

        //creating QName using targetNamespace and name
        QName qname = new QName("http://rabbitmq.javasampleapproach.com/", "PersonServiceImplService");

        Service service = Service.create(wsdlURL, qname);

        //We need to pass interface and model beans to client
        PersonService ps = service.getPort(PersonService.class);

        Person p1 = new Person(); p1.setName("Cuci"); p1.setId(1); p1.setAge(30);
        Person p2 = new Person(); p2.setName("Chili"); p2.setId(2); p2.setAge(25);

        //add person
        System.out.println("Add Person Status="+ps.addPerson(p1));
        System.out.println(Arrays.asList(ps.getAllPersons()));

        //delete person
        System.out.println("Delete Person Status="+ps.deletePerson(2));

        //get all persons
        System.out.println(Arrays.asList(ps.getAllPersons()));



    }

}
