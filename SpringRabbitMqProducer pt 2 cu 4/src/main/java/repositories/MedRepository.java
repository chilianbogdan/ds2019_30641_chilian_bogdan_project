package repositories;

import com.javasampleapproach.rabbitmq.producer.Med;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MedRepository extends JpaRepository<Med, Integer> {
}
