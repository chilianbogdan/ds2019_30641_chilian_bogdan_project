package services;

import dto.PatientDTO;
import dto.PatientViewDTO;
import dto.PatientWithItemsDTO;
import dto.builders.PatientBuilder;
import dto.builders.PatientViewBuilder;
import dto.builders.PatientWithItemsBuilder;
import com.javasampleapproach.rabbitmq.producer.Patient;
import repositories.PatientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PatientService {

    private final PatientRepository patientRepository;

    @Autowired
    public PatientService(PatientRepository patientRepository) {
        this.patientRepository = patientRepository;
    }

    public PatientViewDTO findPatientById(Integer id){
        Optional<Patient> doctor  = patientRepository.findById(id);


        return PatientViewBuilder.generateDTOFromEntity(doctor.get());
    }

    public List<PatientViewDTO> findAll(){
        List<Patient> patients = patientRepository.getAllOrdered();

        return patients.stream()
                .map(PatientViewBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public List<PatientWithItemsDTO> findAllFetch(){
        List<Patient> patientList = patientRepository.getAllFetch();

        return patientList.stream()
                .map(x-> PatientWithItemsBuilder.generateDTOFromEntity(x, x.getMeds()))
                .collect(Collectors.toList());
    }


    //WRONG - without fetch an additional query is executed for each FK
    public List<PatientWithItemsDTO> findAllFetchWrong(){
        List<Patient> patientList = patientRepository.findAll();

        return patientList.stream()
                .map(x-> PatientWithItemsBuilder.generateDTOFromEntity(x, x.getMeds()))
                .collect(Collectors.toList());
    }

    public Integer insert(PatientDTO patientDTO) {


        Patient patient = patientRepository.findByGender(patientDTO.getBirth());


        return patientRepository
                .save(PatientBuilder.generateEntityFromDTO(patientDTO))
                .getId();
    }

    public Integer update(PatientDTO patientDTO) {

        Optional<Patient> person = patientRepository.findById(patientDTO.getId());




        return patientRepository.save(PatientBuilder.generateEntityFromDTO(patientDTO)).getId();
    }

    public void delete(Integer id){
        this.patientRepository.deleteById(id);
    }

}
