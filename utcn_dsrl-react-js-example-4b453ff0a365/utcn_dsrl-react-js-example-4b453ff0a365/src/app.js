import React from 'react'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import NavigationBar from './navigation-bar'
import Home from './home/home';
import Patients from './patient-data/patient/patients'


import styles from './commons/styles/project-style.css';

//let enums = require('./commons/constants/enums');

class App extends React.Component {


    render() {

        return (
            <div className={styles.back}>
            <Router>
                <div>
                    <NavigationBar />
                    <Switch>

                        <Route
                            exact
                            path='/'
                            render={() => <Home/>}
                        />

                        <Route
                            exact
                            path='/Patient'
                            render={() => <Patients/>}
                        />

                        {/*Error*/}

                    </Switch>
                </div>
            </Router>
            </div>
        )
    };
}

export default App
