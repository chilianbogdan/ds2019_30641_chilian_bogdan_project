import React from 'react';

import {Card, Col, Row} from 'reactstrap';
import Table from "../../commons/tables/table"
import PatientForm from "./patient-form";
import PatientDelete from "./patient-delete";
import * as API_USERS from "./api/patient-api"

const columns = [
    {
        Header:  'Name',
        accessor: 'name',
    },
    {
        Header: 'Birth Date',
        accessor: 'birthdate',
    },

];

const filters = [
    {
        accessor: 'name',
    },
    {
        accessor: 'birthdate',
    }
];

class Patients extends React.Component {

    constructor(props){
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.state = {
            collapseForm: true,
            loadPage: false,
            errorStatus: 0,
            error: null
        };

        this.tableData = [];
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    componentDidMount() {
        this.fetchPatients();
    }

    fetchPatients() {
        return API_USERS.getPatients((result, status, err) => {
            console.log(result);
           if(result !== null && status === 200) {
               result.forEach( x => {
                   this.tableData.push({
                       name: x.name,
                       birthdate: x.birthdate,
                   });
               });
               this.forceUpdate();
           } else {
               console.log("Am prins o eroare!!!");
               this.state.errorStatus = status;
               this.state.error = err;
               this.forceUpdate();
           }
        });
    }

    refresh(){
        this.forceUpdate()
    }

    render() {
        let pageSize = 5;
        return (
            <div>
                <Row>
                    <Col>
                        <Card body>
                            <Table
                                data={this.tableData}
                                columns={columns}
                                search={filters}
                                pageSize={pageSize}
                            />
                        </Card>
                    </Col>
                </Row>

                <Row>
                    <Col>
                        <Card body>
                            <div>

                                <PatientForm registerPatient={this.refresh}>

                            </PatientForm>
                                <PatientDelete>

                                </PatientDelete>
                            </div>
                        </Card>
                    </Col>
                </Row>



            </div>
        );
    };

}

export default Patients;
